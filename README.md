# Super Drive Squasher

Super Drive Squasher (SDS) is script to clone a larger drive to a smaller one. Typically used to copy a hard drive to a SSD.

Both MBR and GPT partition tables are supported. 
Drives using LVM, RAID or similar technologies are not supported.